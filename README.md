# Kubernetes Multi-tenancy Test

This is an experiment to see how far we can take hard multi-tenancy with kubernetes right now.

## Test Case

The goal of these policies is to make sure that users cannot disrupt other uses on shared clusters. Our tests involve multiple tenants: `tenant1`, `tenant2` ... `tenant<n>`.

## GCP Setup

Note: The instructions here assume the default zone has already been setup.

### Create cluster

Minimal testing cluster - no need for the default 3 nodes.

```shell
gcloud beta container clusters create --enable-network-policy --enable-pod-security-policy --num-nodes 1 <cluster-name>
```

### Switch kubectl to this cluster

```shell
gcloud container clusters get-credentials <cluster-name>
```

## Global Setup

You must grant your user the ability to create roles in Kubernetes by
running the following command. [USER_ACCOUNT] is the user's email
address:

```sh
kubectl create clusterrolebinding cluster-admin-binding \
--clusterrole cluster-admin --user [USER_ACCOUNT]
```

```shell
# deny all outgoing network by default
kubectl create -f network-policy/default-deny-all-egress.yaml

# requires users to run as an unprivileged user, blocks possible
# escalations to root, and requires use of several security mechanisms.
kubectl create -f pod-security-policy/restricted.yaml
```

NB: GKE already has default `gce.privileged` and
`gce.unprivileged-addon` PodSecurityPolicy.

## Tenant Setup

```shell
# we assume every tenant will have its own namespace for separation
kubectl create -f setup/tenant1_namespace.json
kubectl create -f setup/tenant2_namespace.json

# deny all other projects from connecting directly
kubectl create -f network-policy/tenant1_deny-from-other-namespaces.yaml --namespace=tenant1

# Allow tenant to use restricted psp
kubectl create role tenant1:restricted --verb=use --resource=podsecuritypolicy --resource-name=restricted --namespace=tenant1
kubectl create rolebinding tenant1:restricted --role=tenant1:restricted --serviceaccount=tenant1:default --namespace=tenant1
```

## Create a deployment

```shell
kubectl --namespace=tenant1 create -f- <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ruby-app-deployment
  labels:
    app: ruby-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ruby-app
  template:
    metadata:
      labels:
        app: ruby-app
    spec:
      containers:
      - name: ruby-app
        image: registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image/master/test-dockerfile:853cc9b710f0804157c50b02986cb047614c0a7f
        ports:
        - containerPort: 5000
EOF
```

## Thanks

* https://github.com/ahmetb/kubernetes-network-policy-recipes
